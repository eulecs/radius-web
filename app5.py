from flask import Flask, render_template, request, redirect, url_for
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired
from flask_wtf import FlaskForm
from flask_bootstrap import Bootstrap
import cx_Oracle
from cx_Oracle import DatabaseError
import re
from flask_login import LoginManager, UserMixin, login_user, logout_user, login_required
import mysql.connector

# Configurações do banco de dados MySQL
DB_HOST = 'mysqlserver.fradius_backend'
DB_USER = 'radius'
DB_PASS = 'radpass'
DB_NAME = 'radius'

# Função para obter uma conexão com o banco de dados MySQL
def get_db_connection():
    return mysql.connector.connect(
        host=DB_HOST,
        user=DB_USER,
        password=DB_PASS,
        database=DB_NAME
    )


# Configurações do banco de dados Oracle
DB_USERNAME = 'User'
DB_PASSWORD = 'SenhadoUser'
DB_DSN = 'ipdosenior/senior'

# Função para verificar a matrícula no banco de dados Oracle
def verificar_matricula(matricula):
    try:
        conn = cx_Oracle.connect(DB_USERNAME, DB_PASSWORD, DB_DSN)
        cursor = conn.cursor()
        cursor.execute('SELECT COUNT(*) AS total FROM senior.r034fun WHERE numcad = :matricula', {'matricula': matricula})
        row = cursor.fetchone()
        total_registros = row[0]
        conn.close()

        if total_registros == 0:
            raise ValueError(f"A matrícula {matricula} não foi encontrada no banco de dados.")

        return total_registros
    except DatabaseError as e:
        # Em caso de erro no Oracle, imprima o erro e retorne uma mensagem de erro para exibição na página
        print(f"Erro Oracle: {str(e)}")
        return "Erro ao consultar a matrícula no Oracle."


app = Flask(__name__)
app.secret_key = 'sua_chave_secreta'

bootstrap = Bootstrap(app)
DB_NAME = 'radius'

# Configuração do Flask-Login
login_manager = LoginManager()
login_manager.init_app(app)

# Classe de usuário para Flask-Login
class User(UserMixin):
    def __init__(self, user_id):
        self.id = user_id

    def get_id(self):
        return self.id

@login_manager.user_loader
def load_user(user_id):
    # Implemente a lógica para carregar o usuário a partir do banco de dados ou de onde você armazena as informações dos usuários
    return User(user_id)
    
# MYSQL Cria a tabela no banco de dados se ainda não existir
def create_table():
    conn = get_db_connection()
    cursor = conn.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS mac_addresses
                      (id INT AUTO_INCREMENT PRIMARY KEY,
                       nome VARCHAR(255),
                       cpf VARCHAR(11),
                       matricula VARCHAR(255),
                       mac VARCHAR(17),
                       tipo INT)''')
    conn.commit()
    conn.close()

# Função para obter uma conexão com o banco de dados MySQL
def get_db_connection():
    return mysql.connector.connect(
        host=DB_HOST,
        user=DB_USER,
        password=DB_PASS,
        database=DB_NAME
    )

# Classe de formulário de login
class LoginForm(FlaskForm):
    username = StringField('Usuário', validators=[DataRequired()])
    password = PasswordField('Senha', validators=[DataRequired()])
    submit = SubmitField('Entrar')

# Rota de login
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    error_message = None

    if request.method == 'POST' and form.validate_on_submit():
        username = form.username.data
        password = form.password.data

        # Lógica de autenticação - Exemplo básico
        if username == 'admin' and password == 'password':
            # Autenticação bem-sucedida, redireciona para a página principal
            user = User(user_id=1)
            login_user(user)
            return redirect(url_for('index'))
        else:
            # Autenticação falhou, define a mensagem de erro
            error_message = 'Usuário ou senha inválidos'

    return render_template('login.html', form=form, error_message=error_message)

# Rota principal
@app.route('/')
@login_required
def index():
    conn = get_db_connection()
    cursor = conn.cursor()
    cursor.execute('SELECT id, nome, matricula, mac, tipo FROM mac_addresses;')
    rows = cursor.fetchall()
    
    conn.close()
    return render_template('index.html', mac_addresses = [{'id': row[0], 'nome': row[1], 'matricula': row[2], 'mac': row[3], 'tipo': row[4]} for row in rows])

# Rota de cadastro de Mac Address
@app.route('/cadastrar', methods=['GET', 'POST'])
@login_required
def cadastrar():
    if request.method == 'POST':
        nome = request.form['nome']
        matricula = request.form['matricula']
        mac = request.form['mac']
        tipo = request.form['tipo']
        id = request.form.get('id')

        # Verifica se a matrícula fornecida é válida (contém apenas dígitos)
        if not matricula.isdigit():
            return 'Erro: Matrícula inválida. A matrícula deve conter apenas dígitos.'

        # Tente verificar a matrícula no Oracle
        matricula_valida = verificar_matricula(matricula)

        # Verifique se a matrícula é válida
        if matricula_valida != 1:
            return matricula_valida  # Retorna a mensagem de erro para exibição na página

        # Verifica o número de registros existentes para a matrícula fornecida
        conn = get_db_connection()
        cursor = conn.cursor()
        cursor.execute('SELECT COUNT(*) AS total FROM mac_addresses WHERE matricula = %s', (matricula,))
        row = cursor.fetchone()
        total_registros = row[0]

        # Verifica se já existem dois registros de MAC para a matrícula fornecida
        cursor.execute('SELECT COUNT(*) AS total FROM mac_addresses WHERE matricula = %s AND mac IS NOT NULL', (matricula,))
        row = cursor.fetchone()
        total_mac_registros = row[0]

        # Verifica se já existem dois registros de MAC para a matrícula fornecida, excluindo o registro atual (caso esteja editando)
        if id:
            cursor.execute('SELECT COUNT(*) AS total FROM mac_addresses WHERE matricula = %s AND mac IS NOT NULL AND id != %s', (matricula, id))
        else:
            cursor.execute('SELECT COUNT(*) AS total FROM mac_addresses WHERE matricula = %s AND mac IS NOT NULL', (matricula,))
        row = cursor.fetchone()
        total_mac_registros_excluindo_atual = row[0]

        conn.close()

        # Verifica se já existem dois registros de MAC para a matrícula fornecida
        if total_mac_registros >= 3:
            return 'Erro: Já existem 03 MACs cadastrados para a matrícula {}'.format(matricula)

        # Verifica o formato do MAC
        mac_pattern = re.compile(r'^([0-9A-Fa-f]{2}-){5}[0-9A-Fa-f]{2}$')
        if not mac_pattern.match(mac):
            return 'Erro: Formato de MAC inválido. Use o formato aa-bb-cc-dd-ee-ff'

        # Verifica se o MAC já existe no banco de dados
        conn = get_db_connection()
        cursor = conn.cursor()
        cursor.execute('SELECT COUNT(*) AS total FROM mac_addresses WHERE mac = %s', (mac,))
        row = cursor.fetchone()
        total_mac = row[0]
        if total_mac > 0:
            return 'Erro: Já existe um cadastro com o MAC informado'

        # Verifica se já existe um registro com a mesma matrícula e MAC informados
        if id:
            cursor.execute('SELECT COUNT(*) AS total FROM mac_addresses WHERE matricula = %s AND mac = %s AND id != %s', (matricula, mac, id))
        else:
            cursor.execute('SELECT COUNT(*) AS total FROM mac_addresses WHERE matricula = %s AND mac = %s', (matricula, mac))
        row = cursor.fetchone()
        total_registros = row[0]
        if total_registros > 0:
            return 'Erro: Já existe um cadastro com a matrícula {} e o MAC informado'.format(matricula)

        # Converte o tipo para o valor correspondente
        tipo_valor = None
        if tipo == 'Celular':
            tipo_valor = 98
        elif tipo == 'Notebook':
            tipo_valor = 22
        elif tipo == 'Coletor':
            tipo_valor = 27
            matricula = None  # Define matricula como None para Coletor

        # Insere o registro no banco de dados
        cursor.execute('INSERT INTO mac_addresses (nome, matricula, mac, tipo) VALUES (%s, %s, %s, %s)',
                       (nome, matricula, mac, tipo_valor))
        cursor.execute('INSERT INTO radcheck (username, attribute, op, value) VALUES (%s, %s, %s, %s)', (mac, 'Cleartext-Password', ':=', mac))
        cursor.execute('INSERT INTO radreply (username, attribute, op, value) VALUES (%s, "Tunnel-Type", "=", "VLAN")', (mac,))
        cursor.execute('INSERT INTO radreply (username, attribute, op, value) VALUES (%s, "Tunnel-Medium-Type", "=", "IEEE-802")', (mac,))
        cursor.execute('INSERT INTO radreply (username, attribute, op, value) VALUES (%s, "Tunnel-Private-Group-ID", "=", %s)', (mac, tipo_valor,))
        conn.commit()
        conn.close()

        return redirect(url_for('index'))

    return render_template('cadastrar.html')

# Rota de cadastro de Visitante
@app.route('/cadastrar_visitante', methods=['GET', 'POST'])
@login_required
def cadastrar_visitante():
    if request.method == 'POST':
        nome = request.form['nome']
        cpf = request.form['cpf']
        mac = request.form['mac']

        # Padroniza o CPF, removendo caracteres não numéricos e garantindo 11 dígitos
        cpf = ''.join(filter(str.isdigit, cpf))
        cpf = cpf.zfill(11)

        # Verifica se o CPF fornecido é válido
        if not validar_cpf(cpf):
            return 'Erro: CPF inválido.'

        # Verifica se já existem 2 CPFs cadastrados para o mesmo MAC
        conn = get_db_connection()
        cursor = conn.cursor()
        cursor.execute('SELECT COUNT(*) AS total FROM mac_addresses WHERE cpf = %s', (cpf,))
        row = cursor.fetchone()
        total_cpf_registros = row[0]

        if total_cpf_registros >= 2:
            return 'Erro: Já existem {} CPFs cadastrados para este MAC.'.format(total_cpf_registros)

        # Insere o registro do visitante no banco de dados
        cursor.execute('INSERT INTO mac_addresses (nome, cpf, mac, tipo) VALUES (%s, %s, %s, 98)', (nome, cpf, mac))
        cursor.execute('INSERT INTO radcheck (username, attribute, op, value) VALUES (%s, %s, %s, %s)', (mac, 'Cleartext-Password', ':=', mac))
        cursor.execute('INSERT INTO radreply (username, attribute, op, value) VALUES (%s, "Tunnel-Type", "=", "VLAN")', (mac,))
        cursor.execute('INSERT INTO radreply (username, attribute, op, value) VALUES (%s, "Tunnel-Medium-Type", "=", "IEEE-802")', (mac,))
        cursor.execute('INSERT INTO radreply (username, attribute, op, value) VALUES (%s, "Tunnel-Private-Group-ID", "=", "98")', (mac,))
        conn.commit()
        conn.close()

        return redirect(url_for('index'))

    return render_template('cadastrar_visitante.html')


# Rota de cadastro de Coletor
@app.route('/cadastrar_coletor', methods=['GET', 'POST'])
@login_required
def cadastrar_coletor():
    if request.method == 'POST':
        nome = request.form['nome']
        mac = request.form['mac']

        # Verifica se o MAC já existe no banco de dados
        conn = get_db_connection()
        cursor = conn.cursor()
        cursor.execute('SELECT COUNT(*) AS total FROM mac_addresses WHERE mac = %s', (mac,))
        row = cursor.fetchone()
        total_mac = row[0]

        if total_mac > 0:
            return 'Erro: Já existe um cadastro com o MAC informado.'

        # Insere o registro do coletor no banco de dados
        cursor.execute('INSERT INTO mac_addresses (nome, mac, tipo) VALUES (%s, %s, %s)', (nome, mac, 27))
        cursor.execute('INSERT INTO radcheck (username, attribute, op, value) VALUES (%s, %s, %s, %s)', (mac, 'Cleartext-Password', ':=', mac))
        cursor.execute('INSERT INTO radreply (username, attribute, op, value) VALUES (%s, "Tunnel-Type", "=", "VLAN")', (mac,))
        cursor.execute('INSERT INTO radreply (username, attribute, op, value) VALUES (%s, "Tunnel-Medium-Type", "=", "IEEE-802")', (mac,))
        cursor.execute('INSERT INTO radreply (username, attribute, op, value) VALUES (%s, "Tunnel-Private-Group-ID", "=", "27")', (mac,))

        conn.commit()
        conn.close()

        return redirect(url_for('index'))

    return render_template('cadastrar_coletor.html')

# Função para validar CPF
def validar_cpf(cpf):
    cpf = re.sub(r'[^\d]', '', cpf)  # Remove caracteres não numéricos
    if len(cpf) != 11 or cpf == '00000000000' or cpf == '11111111111' or cpf == '22222222222' or cpf == '33333333333' or cpf == '44444444444' or cpf == '55555555555' or cpf == '66666666666' or cpf == '77777777777' or cpf == '88888888888' or cpf == '99999999999':
        return False
    soma = 0
    for i in range(0, 9):
        soma += int(cpf[i]) * (10 - i)
    resto = 11 - (soma % 11)
    if resto == 10 or resto == 11:
        resto = 0
    if resto != int(cpf[9]):
        return False
    soma = 0
    for i in range(0, 10):
        soma += int(cpf[i]) * (11 - i)
    resto = 11 - (soma % 11)
    if resto == 10 or resto == 11:
        resto = 0
    if resto != int(cpf[10]):
        return False
    return True

# Rota de exclusão de Mac Address
@app.route('/excluir/<int:id>', methods=['GET', 'POST'])
@login_required
def excluir(id):
    conn = get_db_connection()
    cursor = conn.cursor()

    # Primeiro, obtenha o MAC associado a este ID
    cursor.execute('SELECT mac FROM mac_addresses WHERE id = %s', (id,))
    result = cursor.fetchone()

    if result is not None:
        mac = result[0]

        # Agora, exclua o registro da tabela 'radcheck' com base no MAC
        cursor.execute('DELETE FROM radcheck WHERE username = %s', (mac,))
        cursor.execute('DELETE FROM radreply WHERE username = %s', (mac,))

        # Em seguida, exclua o registro da tabela 'mac_addresses' com base no ID
        cursor.execute('DELETE FROM mac_addresses WHERE id = %s', (id,))
    
        conn.commit()
        conn.close()
        return redirect(url_for('index'))
    else:
        # Tratar o caso em que o ID não corresponde a nenhum registro
        conn.close()
        return 'ID não encontrado.', 404  # Retorna uma resposta HTTP 404 (não encontrado)

if __name__ == '__main__':
    create_table()
    app.run(debug=True, host='0.0.0.0')
