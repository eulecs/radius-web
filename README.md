# Radius Web



## Getting started

Este webapp foi desenvolvido com ajuda de IA (ChatGPT e Bard). Ele usa Python e Flask. Se vc for usar isso, tome muito cuidado com as senhas utilizadas e nao publique isso na internet de forma alguma.

## Instalaçao
-Crie a imagem usando o Dockerfile
-Acesse o git oficial do 2stack/freeradius para inicializar o mysql
-Para iniciar basta executar o docker-compose
-Após a inicializaçao dos 3 containers, é necessário configurar a controladora para que os clients passem autenticar no Radius Web
Use o delimitador "-"

## Funcionalidades:
- Consulta matricula num banco de dados Oracle (Senior)
- Utilizao mysql para gravar os dados
- Permite o cadastro de até 03 Mac Address por matricula
- Permite o cadastro de até 02 Mac Address por CPF para o Tipo Visitante
- Permite o cadastro sem CPF e Matricula apenas dispositivo do Tipo Coletor
- Verifica se nao existe Mac Address cadastrado
