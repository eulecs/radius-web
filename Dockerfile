# Use a imagem base Python
FROM python:3.9-slim-buster
WORKDIR /opt/oracle

RUN apt-get update && apt-get install -y libaio1 libpq-dev wget unzip && rm -rf /var/lib/apt/lists/*
RUN wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip && \
    unzip instantclient-basiclite-linuxx64.zip && rm -f instantclient-basiclite-linuxx64.zip && \
    cd /opt/oracle/instantclient* && rm -f *jdbc* *occi* *mysql* *README *jar uidrvci genezi adrci && \
    echo /opt/oracle/instantclient* > /etc/ld.so.conf.d/oracle-instantclient.conf && ldconfig

# Define o diretório de trabalho no contêiner
WORKDIR /app

# Copia os arquivos de especificação de dependências
COPY requirements.txt .

# Instala as dependências do Python
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install flask_wtf --upgrade
RUN pip install flask_login --upgrade
RUN pip install mysql-connector-python

# Copia os arquivos do aplicativo para o contêiner
COPY app5.py .
#COPY mac_addresses.db .
###COPY instantclient_12_2 /Oracle
COPY templates templates
###COPY oracle-instantclient.conf /etc/ld.so.conf.d/

# Define a porta em que o aplicativo irá expor
EXPOSE 5000

# Define a variável de ambiente FLASK_APP
ENV FLASK_APP=app5.py
###RUN export LD_LIBRARY_PATH=/Oracle
###RUN export PATH=/Oracle:$PATH
RUN ldconfig

# Executa o aplicativo Flask quando o contêiner for iniciado
CMD ["flask", "run", "--host=0.0.0.0"]
